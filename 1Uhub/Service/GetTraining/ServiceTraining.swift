//
//  ServiceTraining.swift
//  1Uhub
//
//  Created by Gio Vincent Romero on 10/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import Foundation

import Moya

//import RxMoya

import SwinjectAutoregistration

fileprivate var baseURL = ""

enum ServiceTrainingAPI{
    //GET TRAINING
    case getAddress()
    
}

extension ServiceTrainingAPI: TargetType {
    var parameters: [String : Any]? {
        return nil
    }
    
    
    //API
    
    var baseURL: URL { return URL(string: "https://api-uat.unionbankph.com/ubp/uat/v1/")! }
    
    var path: String {
        switch self {
        case .getAddress:
            return "hrx/profile/address"
            
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getAddress:
            return .get
        }
    }
    
    var sampleData: Data {
        return "".data(using: .utf8)!
    }
    
    
    var task: Task {
        switch self {
        case .getAddress:
            return .request
        }
    }
    
    public var parameterEncoding: ParameterEncoding {
        switch self {
        default:
            if self.method == .get {
                return URLEncoding.default
            }
            return JSONEncoding.default
        }
    }
    
    var headers: [String : String]? {
        
        switch self {
        case .getAddress:
            return ["x-ibm-client-id": "2f65b704-0712-4dc3-988b-168361e49569", "x-ibm-client-secret": "lA5wT2bN0hQ2wD2aY5cS3oV6jK8eJ0eR1cE6bL8qU8mM4jJ3iO","token":"234234234"]
        }
    }
}

class ServiceTraining: MoyaProvider<ServiceTrainingAPI> {
    
    init() {
        super.init()
    }
}

//
//  TrainingEnrollListItem.swift
//  1Uhub
//
//  Created by Gio Vincent Romero on 10/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import UIKit

import IGListKit

import ObjectMapper

class ProfileWorkListItem: NSObject, Mappable {
    
    public var addressType: String
    public var unitNumberBuildingStreet: String
    public var residenceType: String
    public var barangay: String
    public var city: String
    public var province: String
    public var country: String
    public var zipCode: String
    
    /*
     "id": 1,
     "employee": 4,
     "addressType": "Home Address",
     "residenceType": "Owned",
     "unitNumberBuildingStreet": "sample",
     "barangay": "sample",
     "city": "Bangued",
     "province": "Abra",
     "country": "United Arab Emirates",
     "zipCode": "sample",
     "proofOfBilling": "sample"
     
     
     */
    
    public required init?(map: Map) {
        
        addressType = try! map.value("addressType")
        unitNumberBuildingStreet = try! map.value("unitNumberBuildingStreet")
        residenceType = try! map.value("residenceType")
        barangay = try! map.value("barangay")
        city = try! map.value("city")
        province = try! map.value("province")
        country = try! map.value("country")
        zipCode = try! map.value("zipCode")
        
        
    }
    
    public func mapping(map: Map) {
        
        addressType <- map["addressType"]
        unitNumberBuildingStreet <- map["unitNumberBuildingStreet"]
        residenceType <- map["residenceType"]
        barangay <- map["barangay"]
        city <- map["city"]
        province <- map["province"]
        country <- map["country"]
        zipCode <- map["zipCode"]
    }
}

class ProfileAddress : Mappable {
    
    public var currentPage: Int!
    public var totalPages: Int!
    public var totalRecord: Int!
    
    public var data = [ProfileWorkListItem]()
    
    public required init?(map: Map) {
        
        currentPage = try! map.value("currentPage")
        totalPages = try! map.value("totalPages")
        totalRecord = try! map.value("totalRecord")
        
        data = try! map.value("data")
        
    }
    
    public func mapping(map: Map) {
        
        currentPage <- map["currentPage"]
        totalPages <- map["totalPages"]
        totalRecord <- map["nextPage"]
        
        data <- map["data"]
        
    }
    
}

extension ProfileWorkListItem : ListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol {
        return NSNumber(value: hashValue)
        
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let theObject = object as? ProfileWorkListItem else {
            return false
        }
        
        return theObject.hashValue == hashValue
    }
}


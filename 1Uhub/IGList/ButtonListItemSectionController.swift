//
//  ButtonListItemSectionController.swift
//  1Uhub
//
//  Created by Gio Vincent Romero on 06/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import IGListKit

class ButtonListItemSectionController: ListSectionController {
    
    weak var object: ButtonListItem?
    
    var itemSelected: ((ButtonListItem) -> Void)?
    
    override func numberOfItems() -> Int {
        return 1
    }
    
    override func sizeForItem(at index: Int) -> CGSize {
        return CGSize(width: collectionContext!.containerSize.width, height: 60)
    }
    
    override func cellForItem(at index: Int) -> UICollectionViewCell {
        guard let cell = collectionContext!.dequeueReusableCell(withNibName:
            "ButtonListView", bundle: nil, for: self, at: index) as? ButtonListView else {
            fatalError()
        }

        cell.listName = object?.title ?? ""

        return cell
    }
    
    override func didUpdate(to object: Any) {
        
        guard let theObject = object as? ButtonListItem else {
            return
        }
        
        self.object = theObject
    }
    
    override func didSelectItem(at index: Int) {
        
        guard let object = self.object else {
            return
        }
        
        itemSelected?(object)
    }
}

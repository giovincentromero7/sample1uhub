//
//  ButtonListItem.swift
//  1Uhub
//
//  Created by Gio Vincent Romero on 06/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import IGListKit

class ButtonListItem: NSObject {
    
    var ID = ""
    var title = ""
    var subTitle = ""
    
    init(_ ID: String, _ title: String, _ subTitle: String) {
        self.ID = ID
        self.title = title
        self.subTitle = subTitle
    }
}

extension ButtonListItem: ListDiffable {
    
    func diffIdentifier() -> NSObjectProtocol {
        return NSNumber(value: hashValue)
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        
        guard let theObject = object as? ButtonListItem else {
            return false
        }
        
        return theObject.hashValue == hashValue
    }
}

//
//  ViewControllerApproval.swift
//  1Uhub
//
//  Created by Gio Vincent Romero on 06/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import UIKit

import IGListKit

class ViewControllerApproval : BaseViewController {
    
    var tabVC: UITabBarController?
    var forApprovalVC: ViewControllerForApproval?
    var approvalHistoryVC: ViewControllerApprovalHistory?
    
    let EmbedSegueId = "embed"

    @IBOutlet weak var forApprovalButton: SelectionButton!
    @IBOutlet weak var approvalHistoryButton: SelectionButton!
    
    @IBOutlet var tabButtons: Array<SelectionButton>!
    
    @IBAction func didTapButtonForApproval(_ sender: Any) {
        setSelected(button: forApprovalButton)
    }
    
    @IBAction func didTapButtonApprovalHistory(_ sender: Any) {
        setSelected(button: approvalHistoryButton)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    fileprivate func setSelected(button: SelectionButton) {
        button.selectActive = true
        for b in tabButtons {
            if b == button { continue }
            b.selectActive = false
        }
        
        switch button {
        case forApprovalButton:
            tabVC?.selectedIndex = 0
            
        case approvalHistoryButton:
            tabVC?.selectedIndex = 1
            
        default:
            break
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let id = segue.identifier else {
            return
        }
        
        if id == EmbedSegueId, let vc = segue.destination as? UITabBarController {
            tabVC = vc
            tabVC?.tabBar.isHidden = true
            
            forApprovalVC = tabVC?.viewControllers?[0] as? ViewControllerForApproval
            
            approvalHistoryVC = tabVC?.viewControllers?[1] as? ViewControllerApprovalHistory
            
        }
    }
    
}

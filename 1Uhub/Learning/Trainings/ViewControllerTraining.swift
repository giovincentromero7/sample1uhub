//
//  ViewControllerTraining.swift
//  1Uhub
//
//  Created by Gio Vincent Romero on 06/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import UIKit
import IGListKit

class ViewControllerTraining: BaseViewController {
    
    struct segueIds {
        static let borrowABookForm = "borrowABookSegue"
        static let enrollToTrainingForm = "enrollToTrainingSegue"
        static let listenToThePodcastForm = "listenToThePodcastSegue"
        static let startALearningPathwayForm = "startALearningPathwaySegue"
    }
    
    var data: [ListDiffable] = [ListDiffable]()
    
    lazy var adapter: ListAdapter = {
        return ListAdapter(
            updater: ListAdapterUpdater(),
            viewController: self, workingRangeSize: 0
        )
    }()
    
    var learningList: [ListDiffable] = []
    
    var employeeMoreItems : [ListDiffable] = [
        
        ButtonListItem("Borrow", "Borrow A Book", ""),
        ButtonListItem("Enroll", "Enroll To Training", ""),
        ButtonListItem("Listen", "Listen to a Podcast", ""),
        ButtonListItem("Start", "Start a Learning Pathway", ""),
        
        ]
    
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        learningList.append(contentsOf: employeeMoreItems)
        
        setupCollectionView()
        
    }
    
    func setupCollectionView() {
        adapter.collectionView = collectionView
        adapter.dataSource = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let id = segue.identifier else {
            return
        }
        
        if id == segueIds.borrowABookForm {
            //presenter.setTransferType(.otherBanks)
        }
    }

}

extension ViewControllerTraining : ListAdapterDataSource {
    func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
        
        return learningList
    }
    
    func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
        if object is ButtonListItem {
            let section = ButtonListItemSectionController()
            
            section.itemSelected = { item in
                switch item.ID {
                case "Borrow":
                    self.performSegue(withIdentifier: segueIds.borrowABookForm, sender: self)
                    break
                case "Enroll":
                    self.performSegue(withIdentifier: segueIds.enrollToTrainingForm, sender: self)
                    break
                case "Listen":
                    self.performSegue(withIdentifier: segueIds.listenToThePodcastForm, sender: self)
                    break
                case "Start":
                    self.performSegue(withIdentifier: segueIds.startALearningPathwayForm, sender: self)
                    break
                default:
                    break
                }
            }
            return section
        }
        
        return ListSectionController()
    }
    
    func emptyView(for listAdapter: ListAdapter) -> UIView? {
        //add placeholder if list is empty
        return nil
    }
    
    
    
}

//
//  PresenterEnrollToTraining.swift
//  1Uhub
//
//  Created by Gio Vincent Romero on 10/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import RxSwift

//import Moya

protocol PresenterEnrollToTrainingView: class {
    
    func showProfileAddressList(data: [ProfileWorkListItem])
    func showProfileAddressListError(message: String)
}


class PresenterEnrollToTraining {
    
    let getProfileAddressUseCase: GetProfileAddressUseCase!
    
    weak var view: PresenterEnrollToTrainingView?
    
    private var data: [ProfileWorkListItem] = []
    private var profileObject :ProfileAddress!
    
    let disposeBag = DisposeBag()
    
    init(_ getProfileAddressUseCase: GetProfileAddressUseCase) {
        self.getProfileAddressUseCase = getProfileAddressUseCase
    }
    
//    func getTraining(){
//        
//        
//        getProfileAddressUseCase
//            .execute()
//            .subscribe(
//                onNext: { profileObject in
//                    self.data = profileObject.data
//                    print(self.data)
//                    print("data")
//                    
//                    self.view?.showProfileAddressList(data: self.data)
//                    
//            }, onError: { error in
//                
//                self.view?.showProfileAddressListError(message: error.localizedDescription)
//                
//            })
//            .disposed(by: disposeBag)
//    }
    
}

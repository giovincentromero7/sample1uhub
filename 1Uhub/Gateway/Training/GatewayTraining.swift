//
//  GatewayTraining.swift
//  1Uhub
//
//  Created by Gio Vincent Romero on 10/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import Foundation

import RxSwift

import ObjectMapper

/*
 {
 "relationshipId":1,
 "firstName":"Isabel",
 "middleName":"A",
 "lastName":"Jose",
 "birthdate":"1/2/1990",
 "contactNumber":"1234",
 "occupation":"employee",
 "bloodType":"A",
 "suffix":"sample",
 "hmoDependent":"A",
 "insuranceDependent":"A"
 }
 
 
 */

struct ResponseTraining: ImmutableMappable {
    
    var code: Int?
    var id: Int?
    var message: String?
    
    var errors: String?
    
    init(map: Map) throws {
        
        code = try? map.value("code") as Int
        id = try? map.value("id") as Int
        message = try? map.value("message") as String
        
        errors = try? map.value("errors")
    }
    
    func mapping(map: Map) {
        
        code >>> map["code"]
        
        id >>> map["id"]
        message >>> map["message"]
        
        errors >>> map["errors"]
    }
    
}


class GatewayTraining {
    
    let serviceTraining: ServiceTraining
    
    init(_ serviceTraining: ServiceTraining) {
        self.serviceTraining = serviceTraining
    }
    
//    func getProfileAddress() -> Observable<ProfileAddress> {
//        return self.serviceTraining.rx
//            .request(.getAddress())
//            .asObservable()
//            .mapErrors()
//            .mapObject(ProfileAddress.self)
//    }
    
}



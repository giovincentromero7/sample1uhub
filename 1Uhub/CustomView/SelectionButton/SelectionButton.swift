//
//  SelectionButton.swift
//  1Uhub
//
//  Created by Gio Vincent Romero on 11/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import UIKit

@IBDesignable
public class SelectionButton: UIButton {
    
    @IBInspectable public var selectActive:Bool = false {
        didSet {
            let textColor = selectActive ? selectedColorText:unselectedColorText
            self.setTitleColor(textColor, for: .normal)
            
            let backgroundColor = selectActive ?
                selectedBackgroundColor:unselectedBackgroundColor
            self.backgroundColor = backgroundColor
            
        }
    }
    
    @IBInspectable public var selectedActive:Bool = false {
        didSet {
            
            let textColor = selectedActive ? newSelectedColorText:unselectedColorText
            self.setTitleColor(textColor, for: .normal)
            
        }
    }
    
    @IBInspectable public var selectedBackgroundColor:UIColor = UIColor(red: 242/255, green: 166/255, blue: 61/255, alpha: 0.9)
    
    @IBInspectable public var unselectedBackgroundColor:UIColor = UIColor.white.withAlphaComponent(0.4)
    
    //245 //112 //2
    @IBInspectable public var selectedColorText:UIColor = UIColor.white
    
    @IBInspectable public var newSelectedColorText:UIColor = UIColor(red: 242/255, green: 166/255, blue: 61/255, alpha: 0.9)
    
    @IBInspectable public var unselectedColorText:UIColor = UIColor.darkGray.withAlphaComponent(0.4)
    
    
    public override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

//
//  ButtonListView.swift
//  1Uhub
//
//  Created by Gio Vincent Romero on 06/06/2019.
//  Copyright © 2019 Gio Vincent Romero. All rights reserved.
//

import UIKit

class ButtonListView: UICollectionViewCell {

    @IBOutlet weak var listNameLabel: UILabel!
    
    var listName: String = "" {
        didSet {
            listNameLabel.text = listName
        }
    }

}
